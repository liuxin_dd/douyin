## 0.0.1

* initial release.

## 1.0.0

* Update documentation

## 1.0.1

* Fixed the problem that authorization login cannot set the correct scope

## 1.0.2

* Fixed the issue of not receiving callback after authorized login on iOS 
* Specific usage has been updated to README.md[README.md](README.md)

## 1.0.3

* Update douyin android Sdk version